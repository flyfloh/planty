EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Planty"
Date "2020-02-19"
Rev "1.2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+BATT #PWR0101
U 1 1 5D98B9B4
P 3250 950
F 0 "#PWR0101" H 3250 800 50  0001 C CNN
F 1 "+BATT" H 3265 1123 50  0000 C CNN
F 2 "" H 3250 950 50  0001 C CNN
F 3 "" H 3250 950 50  0001 C CNN
	1    3250 950 
	1    0    0    -1  
$EndComp
$Comp
L RF_Module:ESP-12E U2
U 1 1 5D98C5E8
P 3400 4950
F 0 "U2" V 3446 4206 50  0000 R CNN
F 1 "ESP-12E" V 3355 4206 50  0000 R CNN
F 2 "RF_Module:ESP-12E" H 3400 4950 50  0001 C CNN
F 3 "http://wiki.ai-thinker.com/_media/esp8266/esp8266_series_modules_user_manual_v1.1.pdf" H 3050 5050 50  0001 C CNN
	1    3400 4950
	0    -1   -1   0   
$EndComp
$Comp
L Device:CP C1
U 1 1 5D990939
P 3600 1950
F 0 "C1" V 3855 1950 50  0000 C CNN
F 1 "1000uF" V 3764 1950 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D7.5mm_P2.50mm" H 3638 1800 50  0001 C CNN
F 3 "~" H 3600 1950 50  0001 C CNN
	1    3600 1950
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Linear:MCP1700-3302E_TO92 U3
U 1 1 5D98AB4C
P 3450 1400
F 0 "U3" V 3404 1295 50  0000 R CNN
F 1 "MCP1700-3302E_TO92" V 3495 1295 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3450 1200 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001826D.pdf" H 3450 1400 50  0001 C CNN
	1    3450 1400
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5D999D7D
P 3600 2400
F 0 "C2" V 3348 2400 50  0000 C CNN
F 1 "100nF" V 3439 2400 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 3638 2250 50  0001 C CNN
F 3 "~" H 3600 2400 50  0001 C CNN
	1    3600 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	3450 950  3450 1100
Wire Wire Line
	3450 1700 3450 1950
Wire Wire Line
	3450 1950 3450 2400
Connection ~ 3450 1950
Wire Wire Line
	3750 950  3750 1400
Wire Wire Line
	3750 1400 3750 1950
Connection ~ 3750 1400
Wire Wire Line
	3750 1950 3750 2400
Connection ~ 3750 1950
$Comp
L Device:R R2
U 1 1 5D99E0E7
P 3000 3600
F 0 "R2" H 2930 3554 50  0000 R CNN
F 1 "10k " H 2930 3645 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2930 3600 50  0001 C CNN
F 3 "~" H 3000 3600 50  0001 C CNN
	1    3000 3600
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5D99F126
P 4300 4100
F 0 "R3" V 4507 4100 50  0000 C CNN
F 1 "10k" V 4416 4100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 4230 4100 50  0001 C CNN
F 3 "~" H 4300 4100 50  0001 C CNN
	1    4300 4100
	0    -1   -1   0   
$EndComp
Connection ~ 3750 2400
$Comp
L Device:R R1
U 1 1 5D9A6F7B
P 2800 3900
F 0 "R1" H 2730 3854 50  0000 R CNN
F 1 "10k" H 2730 3945 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P1.90mm_Vertical" V 2730 3900 50  0001 C CNN
F 3 "~" H 2800 3900 50  0001 C CNN
	1    2800 3900
	-1   0    0    1   
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 5D9A8F17
P 2600 4250
F 0 "SW1" H 2600 4535 50  0000 C CNN
F 1 "FLASH" H 2600 4444 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 2600 4450 50  0001 C CNN
F 3 "~" H 2600 4450 50  0001 C CNN
	1    2600 4250
	1    0    0    -1  
$EndComp
NoConn ~ 3600 5550
NoConn ~ 3700 5550
$Comp
L Sensor:DHT11 U4
U 1 1 5D97AD0D
P 4000 2900
F 0 "U4" V 3619 2900 50  0000 C CNN
F 1 "DHT11" V 3710 2900 50  0000 C CNN
F 2 "Sensor:Aosong_DHT11_5.5x12.0_P2.54mm" H 4000 2500 50  0001 C CNN
F 3 "http://akizukidenshi.com/download/ds/aosong/DHT11.pdf" H 4150 3150 50  0001 C CNN
	1    4000 2900
	0    -1   1    0   
$EndComp
Wire Wire Line
	4700 4950 4100 4950
$Comp
L bh1750:BH1750 J1
U 1 1 5D9CF3B9
P 5000 4000
F 0 "J1" V 4335 3675 50  0000 C CNN
F 1 "BH1750" V 4426 3675 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 5000 4000 50  0001 C CNN
F 3 "" H 5000 4000 50  0001 C CNN
	1    5000 4000
	0    -1   1    0   
$EndComp
Wire Wire Line
	2800 4350 2800 4250
Connection ~ 2800 4250
Wire Wire Line
	2800 4250 2800 4050
Wire Wire Line
	3000 4350 3000 3750
Wire Wire Line
	3700 4350 3700 4100
Wire Wire Line
	3700 4100 4150 4100
Wire Wire Line
	4450 4100 4700 4100
Connection ~ 4700 4100
Wire Wire Line
	4700 4100 4700 4950
Wire Wire Line
	3500 4350 3500 3600
Wire Wire Line
	3450 2400 3050 2400
Connection ~ 3450 2400
Text Label 3200 2400 0    50   ~ 0
3V3
Wire Wire Line
	2800 3750 2800 3450
Wire Wire Line
	2800 3450 3000 3450
Wire Wire Line
	3000 3450 3000 3300
Connection ~ 3000 3450
Text Label 3000 3350 0    50   ~ 0
3V3
Text Label 4700 3550 0    50   ~ 0
GND
Wire Wire Line
	2400 4250 2400 3800
Text Label 2400 3850 0    50   ~ 0
GND
Text Label 1550 4950 0    50   ~ 0
3V3
Wire Wire Line
	4000 3600 4000 3200
Wire Wire Line
	3500 3600 4000 3600
Wire Wire Line
	4700 2400 4700 2900
Wire Wire Line
	3750 2400 4700 2400
Wire Wire Line
	4300 2900 4700 2900
Connection ~ 4700 2900
Wire Wire Line
	4700 2900 4700 4100
Wire Wire Line
	3700 2900 3450 2900
Wire Wire Line
	3300 4350 3300 3950
Text Label 3300 4100 1    50   ~ 0
SCL
Wire Wire Line
	3200 4350 3200 4100
Text Label 3200 4250 1    50   ~ 0
SDA
Text Label 5250 4100 3    50   ~ 0
SCL
Text Label 5400 4100 3    50   ~ 0
SDA
Text Label 5650 3650 0    50   ~ 0
GND
Text Label 5000 3750 3    50   ~ 0
3V3
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5D9F7266
P 3650 750
F 0 "J3" V 3712 794 50  0000 L CNN
F 1 "Power_Conn" V 3803 794 50  0000 L CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x02_P2.00mm_Vertical" H 3650 750 50  0001 C CNN
F 3 "~" H 3650 750 50  0001 C CNN
	1    3650 750 
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 950  3450 950 
Wire Wire Line
	3250 950  3450 950 
Connection ~ 3450 950 
Wire Wire Line
	3650 950  3750 950 
Wire Wire Line
	3750 950  4500 950 
Connection ~ 3750 950 
Text Label 2250 5800 2    50   ~ 0
RESET
Text Label 3800 4350 0    50   ~ 0
RESET
NoConn ~ 3400 5550
NoConn ~ 3500 5550
NoConn ~ 3800 5550
NoConn ~ 3900 5550
NoConn ~ 3600 4350
NoConn ~ 5650 3750
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DA03FFA
P 3000 900
F 0 "#FLG0101" H 3000 975 50  0001 C CNN
F 1 "PWR_FLAG" H 3000 1073 50  0000 C CNN
F 2 "" H 3000 900 50  0001 C CNN
F 3 "~" H 3000 900 50  0001 C CNN
	1    3000 900 
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5DA042EF
P 4850 900
F 0 "#FLG0102" H 4850 975 50  0001 C CNN
F 1 "PWR_FLAG" H 4850 1073 50  0000 C CNN
F 2 "" H 4850 900 50  0001 C CNN
F 3 "~" H 4850 900 50  0001 C CNN
	1    4850 900 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0103
U 1 1 5DA045A2
P 3000 900
F 0 "#PWR0103" H 3000 750 50  0001 C CNN
F 1 "VCC" H 3018 1073 50  0000 C CNN
F 2 "" H 3000 900 50  0001 C CNN
F 3 "" H 3000 900 50  0001 C CNN
	1    3000 900 
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5DA04921
P 4850 900
F 0 "#PWR0104" H 4850 650 50  0001 C CNN
F 1 "GND" H 4855 727 50  0000 C CNN
F 2 "" H 4850 900 50  0001 C CNN
F 3 "" H 4850 900 50  0001 C CNN
	1    4850 900 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0105
U 1 1 5DA07E5F
P 3250 950
F 0 "#PWR0105" H 3250 800 50  0001 C CNN
F 1 "VCC" H 3268 1123 50  0000 C CNN
F 2 "" H 3250 950 50  0001 C CNN
F 3 "" H 3250 950 50  0001 C CNN
	1    3250 950 
	-1   0    0    1   
$EndComp
Connection ~ 3250 950 
$Comp
L power:GND #PWR0106
U 1 1 5DA08542
P 4500 950
F 0 "#PWR0106" H 4500 700 50  0001 C CNN
F 1 "GND" H 4505 777 50  0000 C CNN
F 2 "" H 4500 950 50  0001 C CNN
F 3 "" H 4500 950 50  0001 C CNN
	1    4500 950 
	1    0    0    -1  
$EndComp
$Comp
L bme280_module:bme280 U1
U 1 1 5DA4C422
P 5200 4350
F 0 "U1" H 5478 4101 50  0000 L CNN
F 1 "bme280" H 5478 4010 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5100 4400 50  0001 C CNN
F 3 "" H 5100 4400 50  0001 C CNN
	1    5200 4350
	1    0    0    -1  
$EndComp
Text Label 5100 4400 2    50   ~ 0
3V3
Text Label 5100 4500 2    50   ~ 0
GND
Text Label 5100 4600 2    50   ~ 0
SCL
Text Label 5100 4700 2    50   ~ 0
SDA
NoConn ~ 5100 4800
NoConn ~ 5100 4900
$Comp
L Device:R R5
U 1 1 5E4DAE75
P 3000 5700
F 0 "R5" H 3070 5746 50  0000 L CNN
F 1 "10k" H 3070 5655 50  0000 L CNN
F 2 "" V 2930 5700 50  0001 C CNN
F 3 "~" H 3000 5700 50  0001 C CNN
	1    3000 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 5650 2800 5550
$Comp
L Device:R R4
U 1 1 5E4DFF1F
P 2250 5100
F 0 "R4" H 2320 5146 50  0000 L CNN
F 1 "10k" H 2320 5055 50  0000 L CNN
F 2 "" V 2180 5100 50  0001 C CNN
F 3 "~" H 2250 5100 50  0001 C CNN
	1    2250 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2400 3450 2900
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5D9ADA41
P 3300 6400
F 0 "J2" H 3218 6075 50  0000 C CNN
F 1 "Moisture" H 3218 6166 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3300 6400 50  0001 C CNN
F 3 "~" H 3300 6400 50  0001 C CNN
	1    3300 6400
	0    1    1    0   
$EndComp
Wire Wire Line
	3200 5550 3200 6200
Wire Wire Line
	3400 6200 4700 6200
Wire Wire Line
	4700 6200 4700 4950
Connection ~ 4700 4950
Wire Wire Line
	3300 6200 3300 6000
Wire Wire Line
	3300 6000 3400 6000
Text Label 3400 6000 0    50   ~ 0
3V3
NoConn ~ 3400 4350
Text Label 3000 5850 3    50   ~ 0
3V3
Wire Wire Line
	2800 5650 2400 5650
Wire Wire Line
	1550 4950 2250 4950
Connection ~ 2250 4950
Wire Wire Line
	2250 4950 2600 4950
Wire Wire Line
	2250 5250 2250 5650
$Comp
L Switch:SW_Push SW2
U 1 1 5E5061CB
P 2400 5850
F 0 "SW2" V 2354 5998 50  0000 L CNN
F 1 "RESET" V 2445 5998 50  0000 L CNN
F 2 "" H 2400 6050 50  0001 C CNN
F 3 "~" H 2400 6050 50  0001 C CNN
	1    2400 5850
	0    1    1    0   
$EndComp
Connection ~ 2400 5650
Wire Wire Line
	2400 5650 2250 5650
Wire Wire Line
	2250 5800 2250 5650
Connection ~ 2250 5650
Wire Wire Line
	2400 6050 2400 6250
Text Label 2400 6250 0    50   ~ 0
GND
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 5E50AC55
P 1850 6450
F 0 "J4" V 2004 6262 50  0000 R CNN
F 1 "Conn_01x02_Male" V 1913 6262 50  0000 R CNN
F 2 "" H 1850 6450 50  0001 C CNN
F 3 "~" H 1850 6450 50  0001 C CNN
	1    1850 6450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 6250 1950 6100
Wire Wire Line
	1850 6250 1850 6100
Text Label 1950 6100 0    50   ~ 0
RX
Text Label 1850 6100 2    50   ~ 0
TX
Text Label 2900 4350 1    50   ~ 0
TX
Text Label 3100 4350 1    50   ~ 0
RX
$EndSCHEMATC
